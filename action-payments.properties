# --------------------------------------------------------------------------------------------
# NOTE : 
# 	This should be used only for DEV instance
# 	For Live instance, all variables should be defined as Environment variables.
# 	Environment variables hold precedence against what is defined in this file
# --------------------------------------------------------------------------------------------

# Allow local configuration to override Remote Externalized configuration
spring.cloud.config.allowOverride=true
# But, only System properties or Env variables (and not local config files) will override externalized configuration
spring.cloud.config.overrideSystemProperties=false

# Disable WAS JMX beans registration
spring.jmx.enabled=false

#############################################################################################
###### Global App Specific Configuration
#############################################################################################
# Logging level (DEBUG,INFO,WARN,FATAL)
logging.level.ROOT=INFO

# to ensure errors logged by Camel RabbitMQConsumer only if severity is ERROR (it helps avoiding lot of noise at WARNING level)
logging.level.org.apache.camel.component.rabbitmq.RabbitMQConsumer=ERROR

# Used by Spring Boot (Application Context)
server.contextPath=/igtb-payments

# Port 51002
server.port=51002

# Module Abbreviation Name (preferably <= 3 characters and only Upper Case Alphabets A-Z)
# e.g. LMS (for Liquidity), IPSH (for Payments), CNR (for Collections & Receivables)...
# This is used for deriving:
# 	- Redis Key name as action-requests:<moduleAbbr>:${channelSeqId} (e.g. action-requests:lms:10581856258-94299189, action-requests:ipsh:..., action-requests:cnr:....)
# 	- Deriving Destination names in Release Trigger and State Update handler modules
ModuleAbbr=IPSH

# Header key and value, to be used as security token while calling APIs for this module
iGTBD-AtomicAPI-SharedKey=I4qwGynNIt5DP5zUjjemHR1mEj8Ii6jq

# Redis Database connection details
Redis.DB.Url=10.233.250.122:6379
Redis.DB.Password=s3cr3t
Redis.DB.SSL.Enabled=false

# To Enable/Disable DevMsg field in JSON response returned from API
# Y - enables DevMsg in output json response (may be useful for dev env)
# N - disables the same (recommended for production env) (this is default, if not specified)
EnableDevMsgInResponse=N

# Camel Messages to be logged at this level (Possible values: ERROR, WARN, INFO, DEBUG, TRACE, OFF)
CamelMessageLoggingLevel=DEBUG

# Release Batch Size
# It is max number of requests to be released per batch (Approved, Retry batch)
ReleaseBatchSize=200

# JMS Broker which is to be used by Commons for handling Release Batch Trigger requests, State Update requests
# Supported values (ActiveMQ / RabbitMQ) - default is ActiveMQ
# This is used for dynamically identifying destination names for subscribing to/publishing Release Batch or State Update events
Digital.JmsBroker.Type=RabbitMQ

# Used as sleep time (in milliseconds) between each message retry, when msg is requeue'd.
# Keep it commented, unless need to override default value.
#MsgRetrySleepTime=250

# Used as max time (in seconds) a message to be retried for in requeue mode
# Keep it commented, unless need to override default value.
#MsgRetryMaxTime=86400


## This is used in Spring Camel beans XML
# Message Broker which is to be used by Commons for Release Batch Triggers, State Update Events and Event Publisher
# Supported value - RabbitMQ
Digital.MsgBroker.Type=RabbitMQ
# This is used by Commons for connecting to RabbitMQ Server
Digital.RabbitMQ.Host=10.233.245.190
Digital.RabbitMQ.Port=5672
Digital.RabbitMQ.User=admin
Digital.RabbitMQ.Password=s3cr3t
Digital.RabbitMQ.VHost=/
Digital.RabbitMQ.SSL.Enabled=false
# Represents DataCenter region, country the process events/data belongs to
DataCenter.Region=
DataCenter.Country=



####
# Represents service key regex pattern (allowed product/subproduct/functions combinations), this module is supposed to 
# process events/data for.
# Pattern should be of the format:  <<product code>>/.<<subproduct code>>:<<function code> 
#
# Product code ::: default is 'paymnt'
# Subproduct codes :: default is '*'(considering all). 
#					List only those business products that are allowed when performing Unified 
#					Fund Transfer, Standing Instructions, Payments Templates 
# Functions codes allowed are as below:: 
# 	- For Fund Transfer:: <<<<<<add, amend, delete, hold, unhold, cancel>>>>>> 
#	- For Standing Instructions ::  <<<<<<add_si, amend_si, hold_si, unhold_si, delete_si>>>>>>>
# 	- For Payments Templates:: <<<<<add_template, amend_template, delete_template>>>>>>
# 
#Digital.ServiceKey.Pattern=paymnt/.*
#Digital.ServiceKey.Patterns=paymnt/ven.*:view,paymnt/ven.*:add
Digital.ServiceKey.Patterns=paymnt/.*:group,paymnt/.*:add,paymnt/.*:hold,paymnt/.*:unhold,paymnt/.*:cancel,paymnt/.*:delete,paymnt/.*:stop_pi,paymnt/.*:add_si,paymnt/.*:amend_si,paymnt/.*:hold_si,paymnt/.*:unhold_si,paymnt/.*:delete_si,paymnt/.*:add_template,paymnt/.*:amend_template,paymnt/.*:delete_template,paymnt/report:generate
####

#############################################################################################
### Exception handling specific log configuration 
#############################################################################################
### Log level-step-up configuration
### Keep it commented, unless there is need to customize
#logging.levelStepUp.CBXS_SYS_BE_REL_RETRY=WARN:0,ERROR:120
#logging.levelStepUp.CBXS_ACT_COM_BE_EVT_REQUEUE=INFO:0,WARN:5,ERROR:120


### DLQ specific action_code's
### Keep it commented, unless there is need to customize
#logging.dlqActionCode.CBXS_SYS_MSG_INVALID=INV_MSG
#logging.dlqActionCode.CBXS_SYS_RETRY_EXCEEDED=OFFLINE
#############################################################################################

#########################################################################
### INFO/HEALTH specific check endpoints of the spring boot application
#########################################################################
eureka.instance.statusPageUrlPath=${server.contextPath}${management.context-path}/info
eureka.instance.healthCheckUrlPath=${server.contextPath}${management.context-path}/health


#######################################################################################
## Backend IPSH Payments 'Validate' and 'Release' REST URIs
#######################################################################################
Backend.ValidateServiceUri=http://ec2-34-202-93-244.compute-1.amazonaws.com:10006
#Backend.ReleaseServiceUri=t3://ec2-34-202-93-244.compute-1.amazonaws.com:10006
Backend.ReleaseServiceUri=tcp://10.233.245.69:61616

#############################################################################################
###### Release Retry Configuration
#############################################################################################
RelRetryCfgIdentifierKeys=FUND_TRANSFER_ANY,STANDING_INSTRUCTION_ANY,PAYMENT_TEMPLATES_ANY,BILL_PAYMENT_ANY

###
# Release Retry Config Identifier key based configuration
# 
# RelRetryCfgIdentifier.<identifierKey>.payloadType=
# RelRetryCfgIdentifier.<identifierKey>.requestType=
# RelRetryCfgIdentifier.<identifierKey>.maxReleaseRetry=
# Where,
#	<payloadType,requestType> together forms a unique key combination for providing related configuration 
#	payloadType = type of the payload e.g. Fundtransfer, LoanAgreement or any
#	requestType = type of the request e.g. create, update, delete or any
# 	maxReleaseRetry = to indicate how many times this message should be retried for releasing before it is marked as Failed
#			(field is optional - default value will be applied)
#
# Configuration is applied in following order of priority, for a combination of <payloadType> and <requestType>:
# 1. <payloadType> and <requestType> exactly match with values provided here.
# 2. Else - <payloadType> and "any"
# 3. Else - "any" and <requestType>
# 4. Else - "any" and "any"
# 5. Else - default retry configuration is applied
#
# FundTransfer 
RelRetryCfgIdentifier.FUND_TRANSFER_ANY.payloadType=FundTransfer
RelRetryCfgIdentifier.FUND_TRANSFER_ANY.requestType=any
RelRetryCfgIdentifier.FUND_TRANSFER_ANY.maxReleaseRetry=

# StandingInstructions 
RelRetryCfgIdentifier.STANDING_INSTRUCTION_ANY.payloadType=StandingInstruction
RelRetryCfgIdentifier.STANDING_INSTRUCTION_ANY.requestType=any
RelRetryCfgIdentifier.STANDING_INSTRUCTION_ANY.maxReleaseRetry=

# PaymentTemplates 
RelRetryCfgIdentifier.PAYMENT_TEMPLATES_ANY.payloadType=TemplateFundTransfer
RelRetryCfgIdentifier.PAYMENT_TEMPLATES_ANY.requestType=any
RelRetryCfgIdentifier.PAYMENT_TEMPLATES_ANY.maxReleaseRetry=

# BillPayments
RelRetryCfgIdentifier.BILL_PAYMENT_ANY.payloadType=BillPayment
RelRetryCfgIdentifier.BILL_PAYMENT_ANY.requestType=any
RelRetryCfgIdentifier.BILL_PAYMENT_ANY.maxReleaseRetry=


##################################################################
### Release Connector Specific Configuration
##################################################################
RelConnector.JMS_CONNECTOR.route=direct:JmsConnectorRoute

######## Release Transformer Routes ########  
## ** Can be uncommented if transformation required during release **
# FundTransfer 
#RelConnector.FUND_TRANSFER_TXFMR.route=direct:TransformReleaseFundTransferRoute
# StandingInstructions 
#RelConnector.STANDING_INSTRUCTION_TXFMR.route=direct:TransformReleaseStandingInstructionRoute
# Payment Templates 
#RelConnector.PAYMENT_TEMPLATES_TXFMR.route=direct:TransformReleaseTemplateFundTransferRoute


######## Backend Release Destination JMS Endpoints ########
# FundTransfer 
#RelConnector.FUND_TRANSFER_JMS.endpoint=custom-jms:queue:${RelConnector.FUND_TRANSFER_JMS.outqueue_endpoint}?disableReplyTo=true
RelConnector.FUND_TRANSFER_JMS.endpoint=jms:queue:${RelConnector.FUND_TRANSFER_JMS.outqueue_endpoint}?disableReplyTo=true

# StandingInstructions 
#RelConnector.STANDING_INSTRUCTION_JMS.endpoint=custom-jms:queue:${RelConnector.STANDING_INSTRUCTION_JMS.outqueue_endpoint}?disableReplyTo=true
RelConnector.STANDING_INSTRUCTION_JMS.endpoint=jms:queue:${RelConnector.STANDING_INSTRUCTION_JMS.outqueue_endpoint}?disableReplyTo=true

# PaymentTemplates 
RelConnector.PAYMENT_TEMPLATES_JMS.endpoint=direct:ReleaseStub
# Bill Payments
#RelConnector.BILL_PAYMENT_JMS.endpoint=custom-jms:queue:${RelConnector.BILL_PAYMENT_JMS.outqueue_endpoint}?disableReplyTo=true
RelConnector.BILL_PAYMENT_JMS.endpoint=jms:queue:${RelConnector.BILL_PAYMENT_JMS.outqueue_endpoint}?disableReplyTo=true


# ActiveMQ configuration
IPSH.ActiveMQ.providerURL=tcp://10.233.245.69:61616
IPSH.ActiveMQ.userName=admin
IPSH.ActiveMQ.password=admin
 
 
######## Release Specific Camel Routing Slips(sequence of routes) ########
# FundTransfer
#RelConnector.FundTransferAny.routingSlip=${RelConnector.JMS_CONNECTOR.route},\
#		${RelConnector.FUND_TRANSFER_JMS.endpoint}
RelConnector.FundTransferAny.routingSlip=${RelConnector.FUND_TRANSFER_JMS.endpoint}



# StandingInstructions
#RelConnector.StandingInstructionAny.routingSlip=${RelConnector.JMS_CONNECTOR.route},\
#		${RelConnector.STANDING_INSTRUCTION_JMS.endpoint}
RelConnector.StandingInstructionAny.routingSlip=${RelConnector.STANDING_INSTRUCTION_JMS.endpoint}


# PaymentTemplates
#RelConnector.TemplateFundTransferAny.routingSlip=${RelConnector.JMS_CONNECTOR.route},\
#        ${RelConnector.PAYMENT_TEMPLATES_JMS.endpoint}
RelConnector.TemplateFundTransferAny.routingSlip=${RelConnector.PAYMENT_TEMPLATES_JMS.endpoint}

# Bill Payments
#RelConnector.BillPaymentAny.routingSlip=${RelConnector.JMS_CONNECTOR.route},\
#        ${RelConnector.BILL_PAYMENT_JMS.endpoint}        
RelConnector.BillPaymentAny.routingSlip=${RelConnector.BILL_PAYMENT_JMS.endpoint}        
       
       
##################################################################
### Validation specific Connector routes
##################################################################
ValConnector.HTTP_CONNECTOR.route=direct:HttpConnectorRoute
ValConnector.BILL_PAYMENT_VAL.route=bean:validateBillPaymentPayloadBean?method=validateRequestPayload
 
 
######## Validate Transformer Routes ########  
## ** Can be uncommented if transformation required during validate **
# FundTransfer
#ValConnector.FUND_TRANSFER_TXFMR.route=direct:TransformValidateFundTransferRoute
# StandingInstructions
#ValConnector.STANDING_INSTRUCTION_TXFMR.route=direct:TransformValidateStandingInstructionRoute
# PaymentTemplates 
#ValConnector.PAYMENT_TEMPLATES_TXFMR.route=direct:TransformValidateTemplateFundTransferRoute

 
######## Backend Validate Destination HTTP REST Endpoints ########
#  ex. ValConnector.FUND_TRANSFER_HTTP.endpoint=jetty://http://localhost:8085/PaymentsAPI/paymentsAPI/v1/validators/transfer
#ValConnector.FUND_TRANSFER_HTTP.endpoint=direct:ValidateStub
ValConnector.FUND_TRANSFER_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/ipshstp/paymentsAPI/v1/validators/transfer

# HoldUnholdCancel(used in case of Fund Transfer)
ValConnector.HOLD_UNHOLD_CANCEL_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/ipshstp/paymentsAPI/v1/validators/huc

# StandingInstructions
ValConnector.STANDING_INSTRUCTION_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/ipshstp/paymentsAPI/v1/validators/stndginstr
#ValConnector.STANDING_INSTRUCTION_HTTP.endpoint=direct:ValidateStub
 
# PaymentTemplates 
# Note:: No backend endpoint required for payment templates since no IPSH connectivity required for templates
ValConnector.PAYMENT_TEMPLATES_HTTP.endpoint=direct:ValidateStub


# BillPayments
ValConnector.BILL_PAYMENT_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/ipshstp/api/bill-payments/validate
ValConnector.BILL_PAYMENT_HOLD_UNHOLD_CANCEL_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/ipshstp/paymentsAPI/v1/validators/huc
 
######## Validate Specific Camel Routing Slips(sequence of routes) ########
# FundTransfer when requestType = 'create'
ValConnector.FundTransferCreate.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
        ${ValConnector.FUND_TRANSFER_HTTP.endpoint}
# FundTransfer when requestType = any other than 'create'        
ValConnector.FundTransferAny.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
        ${ValConnector.HOLD_UNHOLD_CANCEL_HTTP.endpoint}
 
# StandingInstructions
ValConnector.StandingInstructionAny.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
        ${ValConnector.STANDING_INSTRUCTION_HTTP.endpoint}

# PaymentTemplates
ValConnector.TemplateFundTransferAny.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
        ${ValConnector.PAYMENT_TEMPLATES_HTTP.endpoint}

# BillPayments
ValConnector.BillPaymentCreate.routingSlip=${ValConnector.BILL_PAYMENT_VAL.route},\
        ${ValConnector.HTTP_CONNECTOR.route},\
        ${ValConnector.BILL_PAYMENT_HTTP.endpoint}

ValConnector.BillPaymentAny.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
        ${ValConnector.BILL_PAYMENT_HOLD_UNHOLD_CANCEL_HTTP.endpoint} 
 

#################################################################################################
### Backend Instance specific configuration for Camel Component ###
# WLS/WAS ContextFactory, ProviderUrl, JNDIConnectionFactory Name and Destination Queue Names
#################################################################################################
# Backend Server details (Weblogic specific)
Backend.JMS.InitialContextFactory=com.ibm.websphere.naming.WsnInitialContextFactory
Backend.JMS.ProviderURL=${Backend.ReleaseServiceUri}
Backend.JMS.QueueConnectionFactory=jms/stpjmsConnectionFactory

# StateUpdHandler.wlsCamelComponent=BACKEND_JMS_COMPONENT

 RelConnector.FUND_TRANSFER_JMS.outqueue_endpoint=TXN_REL_API_REQ_QUEUE
 RelConnector.STANDING_INSTRUCTION_JMS.outqueue_endpoint=TXN_REL_API_REQ_QUEUE
 RelConnector.HOLD_UNHOLD_CANCEL_JMS.outqueue_endpoint=TXN_REL_API_REQ_QUEUE
 RelConnector.BILL_PAYMENT_JMS.outqueue_endpoint=TXN_REL_API_REQ_QUEUE


#############################################################################################
###### Release Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel JMS Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Release Batch Trigger events 
#RelTriggerHandler.JmsCamelComponent=rabbitmq://${Digital.JmsBroker.Host}:${Digital.JmsBroker.Port}

RelTriggerHandler.CamelComponent=rabbitmq
RelTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Comma separated list of Payload Types, for which this module need to support running Release Batches
RelTriggerHandler.PayloadTypes=FundTransfer,StandingInstruction,SupportingDocument,TemplateFundTransfer,BillPayment


#################################################################################################
###### State Update Handler specific Configuration
#################################################################################################
# Name of the Apache Camel JMS Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to B/E State Update events 
#StateUpdHandler.JmsCamelComponent=rabbitmq://${Digital.JmsBroker.Host}:${Digital.JmsBroker.Port}
StateUpdHandler.CamelComponent=rabbitmq
StateUpdHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30


#################################################################################################
###### Event Publish Handler specific Configuration
#################################################################################################
# Name of the Apache Camel JMS Component (which is configured at Domain level),
# which is to be used by Commons package for publishing Events 
# For RabbitsMQ
MsgPubHandler.CamelComponent=rabbitmq


#############################################################################################
###### Purge Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Purge Batch Trigger events 
PurgeTriggerHandler.CamelComponent=rabbitmq
PurgeTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Minimum number of days the data must be retained in State Store (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.MinRetentionDays=720
# Flag indicating whether purging of IN_PROGRESS requests from State Store is allowed or not (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.PurgeOfInProgressAllowed=false


# Comma separated list of Payload Types, for which this module need to support running Purge Batches
PurgeTriggerHandler.PayloadTypes=FundTransfer,StandingInstruction,SupportingDocument,TemplateFundTransfer,BillPayment
 
 
##################################################################
### Zipkin specific options
##################################################################
spring.zipkin.enabled=false
spring.sleuth.enabled=false
#spring.sleuth.web.additionalSkipPattern=.*/getDetails|.*/updateDetails
#spring.sleuth.sampler.probability=1.0
#spring.zipkin.baseUrl=http://localhost:9411
#--------------------------------------------------------------------------------------------------------
#################################################################################################
### State Management controls for draft and initiate states for unhappy validation scenarios
#################################################################################################
# Flag to indicate if initiate request should be marked as failed (500) if validate call gets timed out. Defaults to false. 
StateMgmt.failInitiateOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if validate call gets timed out during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if there are validation failure (400,422) during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValFailure=true
#################################################################################################